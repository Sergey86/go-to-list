﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace to_do_list
{
    public class Task
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Finished { get; set; }
        public Task() { }
        public Task(string description)
        {
            Description = description;
        }
        public Task(string description,bool finished) : this(description)
        {
            Finished = finished;
        }
        public override string ToString()
        {
            return this.Description;
        }
    }
}
