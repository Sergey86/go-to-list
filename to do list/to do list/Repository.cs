﻿using System.Collections.Generic;
using System.Linq;

namespace to_do_list
{
    public class RepositoryData
    {

        public void AddTask(Task task)
        {
            using (var dataBase = new DataBase())
            {
                dataBase.Tasks.Add(task);
                dataBase.SaveChanges();
            }
        }

        public void RemoveTask(int id)
        {
            using (var dataBase = new DataBase())
            {
                Task task = dataBase.Tasks.Find(id);
                dataBase.Tasks.Remove(task);
                dataBase.SaveChanges();
            }
        }

        public List<Task> GetAllTask()
        {
            return new DataBase().Tasks.ToList();
        }
        public void RefreshTask(Task task)
        {
            using(var dataBase = new DataBase())
            {
                Task currentTask = dataBase.Tasks.Find(task.Id);
                dataBase.Entry<Task>(currentTask).CurrentValues.SetValues(task);
                dataBase.SaveChanges();
            }
        }
    }
}
