﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace to_do_list
{
    public class DataBase : DbContext
    {
        public DataBase() : base("to_do_list.Database1")
        {
            Database.SetInitializer<DataBase>(new CreateDatabaseIfNotExists<DataBase>());
            Database.SetInitializer<DataBase>(new DropCreateDatabaseIfModelChanges<DataBase>());
        }
        public DbSet<Task> Tasks { get; set; }
    }
}
