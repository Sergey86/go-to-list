﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace to_do_list
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonSaveClick(object sender, EventArgs e)
        {
            RepositoryData repositoryData = new RepositoryData();
            if (FieldText.Text != "")
            {
                Task task = new Task(FieldText.Text);
                repositoryData.AddTask(task);
                checkedListBox1.Items.Add(task);
                FieldText.Focus();
                FieldText.Clear();
            }
            else
            {
                MessageBox.Show("Please enter a task to add", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ButtonRemoveClick(object sender, EventArgs e)
        {
            RepositoryData repositoryData = new RepositoryData();
            if (checkedListBox1.SelectedIndex >= 0)
            {                
                Task task = checkedListBox1.Items[checkedListBox1.SelectedIndex] as Task;
                if (task != null)
                {
                    repositoryData.RemoveTask(task.Id);
                    checkedListBox1.Items.RemoveAt(checkedListBox1.SelectedIndex);
                }                
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FieldText.Focus();
            PopulateCheckedListBox();
            
        }

        private void PopulateCheckedListBox()
        {
            RepositoryData repositoryData = new RepositoryData();
            IEnumerable<Task> tasks = repositoryData.GetAllTask();
            checkedListBox1.Items.AddRange(tasks.ToArray<object>());
            for (var i = 0; i < tasks.Count(); i++)
            {
                checkedListBox1.SetItemChecked(i, tasks.ElementAt(i).Finished);
            }
        }
                
        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            RepositoryData repositoryData = new RepositoryData();
            if (checkedListBox1.SelectedIndex >= 0)
            {
                Task task = checkedListBox1.Items[e.Index] as Task;
                if (task != null)
                {
                    CheckState state = e.NewValue;
                    if (state == CheckState.Checked)
                    {
                        task.Finished = true;
                    }
                    else
                    {
                        task.Finished = false;
                    }
                    repositoryData.RefreshTask(task);
                }
            }
        }
    }
}
